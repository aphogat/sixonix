from .run import run

BENCHMARKS = [
    "manhattan",
    "manhattan_o",
    "car_chase",
    "car_chase_o",
    "trex",
    "trex_o",
    "fill",
    "fill_o",
    "tess",
    "tess_o",
    "alu2",
    "alu2_o",
    "driver2",
    "driver2_o",
]
